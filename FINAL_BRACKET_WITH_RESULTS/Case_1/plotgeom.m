function plotgeom(map,COORD,rho,nElements,h)
%...plots a 3 dimentional geometry with hexahedral mesh.
%.....GHAENENDRA KUMAR DAS, AE, UIUC
%.........................................................

%  a=COORD(:,2);     %for vertical alignment, swap y and z  
%  COORD(:,2)=COORD(:,3);    %make sure to change axis label if
% % uncomemmented
% COORD(:,3)=a;

 COORD(:,3)=-COORD(:,3);        %z direction flipped
 f=[1 2 3 4;
    2 6 7 3;
    4 3 7 8;
    1 5 8 4;
    1 2 6 5;
    5 6 7 8];

% I=find(rho>=h); %find elements with rho>=f
figure()
for nn=1:nElements 
%     e=I(nn);        %extract element
    connect=map(nn,:);   %extract connectivity
    c=rho(nn);
    
    for i=1:6
      if c>1
         c=0.9999999; %rho=1 gives error
      elseif c<h
          c=0;
     end
    index=f(i,:);
    Faces=connect(index);
    patch('Faces',Faces,'Vertices',COORD,'EdgeAlpha',c,'facealpha',c,'facecolor','b') %0.2*[c c c]
   
   hold on
    end
    hold on
    
    
end
axis equal; axis tight; axis on; box on; view([130,48]);
   xlabel('x')
ylabel('y')
zlabel('z')


end





