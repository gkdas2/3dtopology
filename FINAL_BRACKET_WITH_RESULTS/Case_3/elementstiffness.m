function [ke0,vole]=elementstiffness(map,COORD,nElements,mu) 

DM=1/((1+mu)*(1-2*mu))*[1-mu mu mu 0 0 0;
                         mu 1-mu mu 0 0 0;
                         mu mu 1-mu 0 0 0;
                         0 0 0 (1-2*mu)/2 0 0;
                         0 0 0 0 (1-2*mu)/2 0;
                         0 0 0 0 0 (1-2*mu)/2];
                     

vole=zeros(nElements,1);
ke0(:,:,nElements)=zeros(24);
for i=1:nElements
  nodes=map(i,:);  %extract nodes for each element from connectivity matrix
  gcoord=COORD(nodes,:);    %extract corresponding (x,y) coordinates for the nodes

  [B1,det_J1,~]=shapefunction(-sqrt(1/3),-sqrt(1/3),-sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [B2,det_J2,~]=shapefunction(sqrt(1/3),-sqrt(1/3),-sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [B3,det_J3,~]=shapefunction(sqrt(1/3),sqrt(1/3),-sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [B4,det_J4,~]=shapefunction(-sqrt(1/3),sqrt(1/3),-sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
 [B5,det_J5,~]=shapefunction(-sqrt(1/3),-sqrt(1/3),sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [B6,det_J6,~]=shapefunction(sqrt(1/3),-sqrt(1/3),sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [B7,det_J7,~]=shapefunction(sqrt(1/3),sqrt(1/3),sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [B8,det_J8,~]=shapefunction(-sqrt(1/3),sqrt(1/3),sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
w=1; %weight
% ke01=w*B1'*Emin*B1*det_J1+w*B2'*Emin*B2*det_J2+w*B3'*Emin*B3*det_J3+w*B4'*Emin*B4*det_J4+...
%      w*B5'*Emin*B5*det_J5+w*B6'*Emin*B6*det_J6+w*B7'*Emin*B7*det_J7+w*B8'*Emin*B8*det_J8;
 ke0(:,:,i)=w*B1'*(DM)*B1*det_J1+w*B2'*(DM)*B2*det_J2+w*B3'*(DM)*B3*det_J3+w*B4'*(DM)*B4*det_J4+...
     w*B5'*(DM)*B5*det_J5+w*B6'*(DM)*B6*det_J6+w*B7'*(DM)*B7*det_J7+w*B8'*(DM)*B8*det_J8;

vole(i)=abs(det_J1+det_J2+det_J3+det_J4+det_J5+det_J6+det_J7+det_J8);    
end