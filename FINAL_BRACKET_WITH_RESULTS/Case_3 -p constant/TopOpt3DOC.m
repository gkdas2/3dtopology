%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3D Topology Optimization
% Optimality Condition method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%..........GHANENDRA KUMAR DAS, AE ,UIUC......
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

% Case III

clear ;


%-------INPUT--------%
Volfrac=0.5;    %Volume fraction
r=15;            %min filter radius
p=1;            %SIMP Penalty
q=1;            %Density filter weigth penalty
E0=200;           %Young's Modulus 
mu=0.27;         %Poisson Ratio
Emin=1e-9;      %Minimum Stiffness matrix

disp(['Volume fraction=',num2str(Volfrac)])
disp(['filter radius=',num2str(r)])
disp(['SIMP Penalty=',num2str(p)])
disp(['Density Filter Penalty=',num2str(q)])


%----------------Load Mesh Data-------------------%
disp('Loading mesh...');
load coord.m   %coordinates
load connect.m %Element connectivity
load fixednodes.m   %fixed nodes
load cloadL.m      %nodes on left lug for force applied
load cloadR.m      %nodes on right lug for force applied
load fixedelements.m  % elements around the hole that must be present at all times

%------------Prepare Domain---------%                 
COORD= coord(:,[2 3 4]);        %XYZ coordinates
nNodes=size(COORD,1);           %Number of nodes
dof=3;                          %per node
ndof=1:dof*nNodes;              %All dof index
map=connect(:,2:9);             %Connectivity matrix
nElements=size(map,1);          %Number of elements

cloadnodeL=cloadL(:,1);           %Concentrated load nodes
cloaddofL=cloadL(:,2);            %Concentrated load dof
cloadmagL=cloadL(:,3);            %Concentrated load values

cloadnodeR=cloadR(:,1);           %Concentrated load nodes
cloaddofR=cloadR(:,2);            %Concentrated load dof
cloadmagR=cloadR(:,3);            %Concentrated load values

F=sparse(nNodes*dof,1);         %Load vector initialize
F(3*cloadnodeL-(3-cloaddofL),1)=cloadmagL; %Fill load vector
F(3*cloadnodeR-(3-cloaddofR),1)=cloadmagR; %Fill load vector

clearvars cload coord connect   %Dont need these variables anymore

%--------design variables intial value: rho ------%
rho=Volfrac*ones(nElements,1);     

%-------Calculate density filter-----------%
disp('Calculating Density filter matrix')
Wbar=DensityFilter3D(map,COORD,r,q);

disp('Calculating element stiffness calculation with E=1') 
[ke0,vole]=elementstiffness(map,COORD,nElements,mu);
Fullvol=Volfrac*sum(vole);      %Domain volume 
%-------Begin Optimization-----------------%
disp('Begin Optimization...')

loop = 0; 
change = 1.;
Obj=nan(1000,1);    %Initialize large space for objective 
 % START ITERATION
 while change > 0.01  
  loop = loop + 1;
  rhoPhys = rho;
   
  if change<0.02 && p<3
      p=3;
  end
      
  
  
%    if loop==10, p=1.5;
%    elseif loop==20, p=2;
%    elseif loop==30, p=2.5;
%    elseif loop==40, p=3;
% %   elseif loop==130, p=4;
%    end
  
%   	if change<0.02 && p<3
% 	p=p+0.1;		
%     end
  [Obj(loop),gradphys]=FEA3D(map,fixednodes,F,nElements,nNodes, Emin,E0,rhoPhys,p,ke0); %FE Analysis
  
  gradd=Wbar*gradphys;  %Convert filtered sensitivity back to actual
     
  DC=Wbar*vole; %Convert filtered volume constraint sensitivity back to actual
  
  % Density Update by Optimality Critera method
    l1 = 0; l2 = 1e9; move = 0.2;
    while (l2-l1)/(l1+l2) > 1e-3
     lmid = 0.5*(l2+l1);
     rhonew= max(0,max(rho-move,min(1,min(rho+move,rho.*sqrt(-gradd./DC/lmid)))));
     rhonew(fixedelements)=1;   %Ensure that these elements are always present
     rhoPhys= Wbar*rhonew; %Filter density

     if sum(rhoPhys.*vole)>Fullvol 
     l1 = lmid; 
     else
     l2 = lmid;
     end
    end
    
    change = max(abs(rhonew(:)-rho(:))); %change in density parameter
     rho = rhonew; %Update rho for next iteration
 
    %---Display progress-----%
     fprintf(' It.:%5i  Obj.:%11.8f    Vol.:%7.4f    ch.:%7.4f   p:%7.4f \n',loop,Obj(loop), ...
     mean(rhoPhys(:)),change,p);
 
 end
 
 disp('Optimization converged')
 save('Results')
 save rhoPhys



h=0.5;
 plotgeom(map,COORD,rhoPhys,nElements,h) %h=rho threshold to plot
 title('Optimized design')