clc;
clear all;
close all;
format long
%-------- load all geometry definition------------------------
load coord.m  %nodes and coordinates  
load connect.m %element and connectivity matrix
load fixednodes.m %nodes with Encastre BCs
load udl_elements.m %elements with udl applied
load udl_nodes.m %nodes of the udl_elements with udl applied


E0=210e9;
mu=0.3;
E=E0/((1+mu)*(1-2*mu))*[1-mu mu mu 0 0 0;
                         mu 1-mu mu 0 0 0;
                         mu mu 1-mu 0 0 0;
                         0 0 0 (1-2*mu)/2 0 0;
                         0 0 0 0 (1-2*mu)/2 0;
                         0 0 0 0 0 (1-2*mu)/2];
%----------arrange data------------

elIndex=connect(:,1)';
nElements=max(size(elIndex));

nodeIndex=coord(:,1)';
nNodes=max(size(nodeIndex));

dof=3;%per node
ndof=1:dof*nNodes;

COORD= coord(:,[2 3 4]);

map=connect(:,[2:9]);

%Visualization
[t,tnorm]=MyRobustCrust(COORD);
hold on; 
trisurf(t,COORD(:,1),COORD(:,3),COORD(:,2),'facecolor','k','edgecolor','c'),view(-7,3);
axis tight
grid on
xlabel('x');
ylabel('z');
zlabel('y');
title('Original shape');
hold off
% figure
% title('Geometry- not representative of actual hex mesh','fontsize',14);
% trisurf(t,COORD(:,1),COORD(:,2),COORD(:,3),'facecolor','k','edgecolor','c'),view(0,-100);
% grid on
% figure
% nodes=map(:,:);
% gcoord=COORD(nodes,:);
% plot3(gcoord(:,1),gcoord(:,2),gcoord(:,3),'o','color','k'),view (120,120);
% grid on
% axis 

%------Initialize-----------

K=sparse(nNodes*dof,nNodes*dof);
U=sparse(nNodes*dof,1);
F=sparse(nNodes*dof,1);

%-------free and fixed dofs-------
fixeddof=[3*fixednodes;3*fixednodes-1;3*fixednodes-2];
fixeddof=sort(fixeddof)';
freedof=setdiff(ndof,fixeddof);
U(fixeddof,1)=0;

%----------------------
P=[0;1;0]; %udl magnitude, N/m2;


%----------Begin calculating K matrix for FE Analysis--------------%
for i=1:nElements
  nodes=map(i,:);   %extract nodes for each element from connectivity matrix
  gcoord=COORD(nodes,:);    %extract corresponding (x,y) coordinates for the nodes
  [B1(:,:,i),det_J1(i),N1(:,:,i)]=shapefunction(-sqrt(1/3),-sqrt(1/3),-sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [B2(:,:,i),det_J2(i),N2(:,:,i)]=shapefunction(sqrt(1/3),-sqrt(1/3),-sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [B3(:,:,i),det_J3(i),N3(:,:,i)]=shapefunction(sqrt(1/3),sqrt(1/3),-sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [B4(:,:,i),det_J4(i),N4(:,:,i)]=shapefunction(-sqrt(1/3),sqrt(1/3),-sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
 [B5(:,:,i),det_J5(i),N5(:,:,i)]=shapefunction(-sqrt(1/3),-sqrt(1/3),sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [B6(:,:,i),det_J6(i),N6(:,:,i)]=shapefunction(sqrt(1/3),-sqrt(1/3),sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [B7(:,:,i),det_J7(i),N7(:,:,i)]=shapefunction(sqrt(1/3),sqrt(1/3),sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [B8(:,:,i),det_J8(i),N8(:,:,i)]=shapefunction(-sqrt(1/3),sqrt(1/3),sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
t=1; %weight
 ke=t*B1(:,:,i)'*E*B1(:,:,i)*det_J1(i)+t*B2(:,:,i)'*E*B2(:,:,i)*det_J2(i)+t*B3(:,:,i)'*E*B3(:,:,i)*det_J3(i)+t*B4(:,:,i)'*E*B4(:,:,i)*det_J4(i)+...
     t*B5(:,:,i)'*E*B5(:,:,i)*det_J5(i)+t*B6(:,:,i)'*E*B6(:,:,i)*det_J6(i)+t*B7(:,:,i)'*E*B7(:,:,i)*det_J7(i)+t*B8(:,:,i)'*E*B8(:,:,i)*det_J8(i);
 
 f=t*N5(:,:,i)'*P+t*N6(:,:,i)'*P+t*N7(:,:,i)'*P+t*N8(:,:,i)'*P;
index=[];
  for jj=1:8
      index=[index,3*nodes(jj)-2,3*nodes(jj)-1,3*nodes(jj)];  %Determine correct positiom for the ith element's local ke in Global K matrix
  end
    K(index,index)=K(index,index)+ke;     %Insert local ke in Global K matrix at correct position
     F(index,1)=F(index,1)+f;
end

% for ii=901:1012
%     nodess=map(i,:);
%     t=1;
%     f=t*N5(:,:,i)'*P+t*N6(:,:,i)'*P+t*N7(:,:,i)'*P+t*N8(:,:,i)'*P;
% indexx=[];
%   for jj=1:8
%       indexx=[indexx,3*nodess(jj)-2,3*nodess(jj)-1,3*nodess(jj)];  %Determine correct positiom for the ith element's local ke in Global K matrix
%   end
   
%    F(indexx,1)=F(indexx,1)+f;
% end
    
    
   
hold on
    
%----------FE Analysis------------------
U(freedof,1)=K(freedof,freedof)\F(freedof);
%-----------
xnew=COORD(:,1)+U(1:3:max(size(U)));      %determine new x coordinates of the nodes
ynew=COORD(:,2)+U(2:3:max(size(U)));      %determine new y coordinates of the nodes
znew=COORD(:,3)+U(3:3:max(size(U))); 

COORD_NEW=[xnew, ynew, znew];
%Visualisation

[t,tnorm]=MyRobustCrust(COORD_NEW);
hold on; 

figure
trisurf(t,COORD_NEW(:,1),COORD_NEW(:,3),COORD_NEW(:,2),'facecolor','k','edgecolor','r'),view(-7,3);
grid on
xlabel('x');
ylabel('z');
zlabel('y');
title('Deformed shape');
axis tight