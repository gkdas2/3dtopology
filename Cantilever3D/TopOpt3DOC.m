%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3D Topology Optimization
% Optimality Condition method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%..........GHANENDRA KUMAR DAS, AE ,UIUC......
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

clear ;


%-------INPUT--------%
Volfrac=0.5;    %Volume fraction
r=2;            %min filter radius
p=3;            %SIMP Penalty
q=1;            %Density filter weigth penalty
E0=1;           %Young's Modulus 
mu=0.3;         %Poisson Ratio
Emin=1e-9;      %Minimum Stiffness matrix

disp(['Volume fraction=',num2str(Volfrac)])
disp(['filter radius=',num2str(r)])
disp(['SIMP Penalty=',num2str(p)])
disp(['Density Filter Penalty=',num2str(q)])


%----------------Load Mesh Data-------------------%
disp('Loading mesh...');
load coord.m
load connect.m
load fixednodes.m
load cload.m

%------------Prepare Domain---------%                 
COORD= coord(:,[2 3 4]);        %XYZ coordinates
nNodes=size(COORD,1);           %Number of nodes
dof=3;                          %per node
ndof=1:dof*nNodes;              %All dof index
map=connect(:,2:9);             %Connectivity matrix
nElements=size(map,1);          %Number of elements

cloadnode=cload(:,1);           %Concentrated load nodes
cloaddof=cload(:,2);            %Concentrated load dof
cloadmag=cload(:,3);            %Concentrated load values
F=sparse(nNodes*dof,1);         %Load vector initialize
F(3*cloadnode-(3-cloaddof),1)=cloadmag; %Fill load vector
clearvars cload coord connect   %Dont need these variables anymore

%--------design variables intial value: rho ------%
rho=Volfrac*ones(nElements,1);     

%-------Calculate density filter-----------%
disp('Calculating Density filter matrix')

Wbar=DensityFilter3D(map,COORD,r,q);

 
%-------Begin Optimization-----------------%
disp('Begin Optimization...')

loop = 0; 
change = 1.;
 % START ITERATION
 while change > 0.01  
  loop = loop + 1;
  rhoPhys = rho;
  [Obj,gradphys,vole]=FEA3D(map,COORD,fixednodes,F,nElements,nNodes, Emin,E0,mu,rhoPhys,p); %FE Analysis
  Fullvol=Volfrac*sum(vole);      %Domain volume 
  gradd=Wbar*gradphys;  %Convert filtered sensitivity back to actual
     
  DC=Wbar*vole; %Convert filtered volume constraint sensitivity back to actual
  
  % Density Update by Optimality Critera method
    l1 = 0; l2 = 1e9; move = 0.2;
    while (l2-l1)/(l1+l2) > 1e-3
     lmid = 0.5*(l2+l1);
     rhonew= max(0,max(rho-move,min(1,min(rho+move,rho.*sqrt(-gradd./DC/lmid)))));
 
     rhoPhys= Wbar*rhonew; %Filter density

     if sum(rhoPhys.*vole)>Fullvol 
     l1 = lmid; 
     else
     l2 = lmid;
     end
    end
    
    change = max(abs(rhonew(:)-rho(:))); %change in density parameter
     rho = rhonew; %Update rho for next iteration
 
    %---Display progress-----%
     fprintf(' It.:%5i  Obj.:%11.8f    Vol.:%7.4f    ch.:%7.4f   p:%7.4f \n',loop,Obj, ...
     mean(rhoPhys(:)),change,p);
 
 end
 
 disp('Optimization converged')
 save('Results')
 save rhoPhys




 plotgeom(map,COORD,nElements,rhoPhys)
 title('Optimized design')