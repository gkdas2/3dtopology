function plotgeom(map,COORD,nElements,rho)

 f=[1 2 3 4;
    2 6 7 3;
    4 3 7 8;
    1 5 8 4;
    1 2 6 5;
    5 6 7 8];
for nn=1:nElements 
    connect=map(nn,:);
    c=rho(nn);
    if c<0.001
        c=0;
    end
    
    for i=1:6
     
    index=f(i,:);
    Faces=connect(index);
    patch('Faces',Faces,'Vertices',COORD,'Facecolor',(1-c)*[1 1 1])
   
   hold on
    end
    hold on
    
    
end
end




