%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3D Topology Optimization
% Optimality Condition method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%..........GHANENDRA KUMAR DAS, AE ,UIUC......
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

clc
clear ;close all;
format short
%----------------Load Mesh Data-------------------%
disp('Loading mesh...');
load coord.m
load connect.m
load fixednodes.m
load cload.m


%------------Prepare Domain---------%
r=1.5;     %Filter radius
disp('filter radius=')
disp(r)



E0=1;
mu=0.3;
Emin=1e-9;

                    





COORD= coord(:,[2 3 4]);
nNodes=size(COORD,1);
dof=3;%per node
ndof=1:dof*nNodes;
map=connect(:,2:9);
nElements=size(map,1); 


cloadnode=cload(:,1);
cloaddof=cload(:,2);
cloadmag=cload(:,3); 
F=sparse(nNodes*dof,1);
F(cloaddof.*cloadnode,1)=cloadmag;


rho=0.4*ones(nElements,1);     %design variables: rho 
%---------------------------------120x20 mesh-----------------------%

clearvars cload coord connect %Dont need these variables

disp('Calculating Density filter matrix')
Wbar=DensityFilter3D(map,COORD,r);

p=3;  %Penalty


 
Volfrac=0.3;
% Fullvol=Volfrac*sum(vole);      %Domain volume
 disp('Begin Optimization...')
% 





loop = 0; 
change = 1.;
 % START ITERATION
 while change > 0.01  
  loop = loop + 1;
  rhoPhys = rho;
  [Obj,gradphys,vole]=FEA3D(map,COORD,fixednodes,F,nElements,nNodes, Emin,E0,mu,rhoPhys,p); %FE Analysis
  Fullvol=Volfrac*sum(vole);      %Domain volume 
  gradd=Wbar*gradphys;  %Convert filtered sensitivity back to actual
     
  DC=Wbar*vole; %Convert filtered sensitivity back to actual
  
  % Density Update by Optimality Critera method
    l1 = 0; l2 = 1e9; move = 0.2;
    while (l2-l1)/(l1+l2) > 1e-3
     lmid = 0.5*(l2+l1);
     rhonew= max(0,max(rho-move,min(1,min(rho+move,rho.*sqrt(-gradd./DC/lmid)))));
 
     rhoPhys= Wbar*rhonew; %Filter density

     if sum(rhoPhys.*vole)-Fullvol>0 
     l1 = lmid; 
     else
     l2 = lmid;
     end
    end
    
    change = max(abs(rhonew(:)-rho(:))); %change in density parameter
     rho = rhonew; %Update rho for next iteration
 
     % Plot density 
%     myfig=figure(1);
%     rhomatrix=reshape(rhoPhys,120,20);
%     rhomatrix=rhomatrix';
%     rhomatrix=flip(rhomatrix);
%     colormap(gray)
%     imagesc(-rhomatrix)
%     colorbar;
%     drawnow
%     hold on
%     axis equal
% 
     %Display progress
     fprintf(' It.:%5i  Obj.:%11.4f    Vol.:%7.3f    ch.:%7.3f\n',loop,Obj, ...
     mean(rhoPhys(:)),change);
 end
 
 disp('Optimization converged')
 save('Results')
save rhoPhys

 
% title('Optimized design')

%  plotgeom(map,COORD,nElements,rhoPhys)