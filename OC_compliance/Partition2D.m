function [Kpp,Kpf,Kfp,Kff,K]= Partition2D(K,fixeddof,freedof)
%.......................................
% Parition2D... Takes 2D matrix and partitions into fixed and free dof
% blocks
%........................................................................
% K=[1 2 3 4 5 6;
%    7 8 9 10 11 12;
%    13 14 15 16 17 18;
%    19 20 21 22 23 24;
%    25 26 27 28 29 30;
%    31 32 33 34 35 36];


% fixeddof=[1 2 5];
% freedof=[3 4 6];

Kpp=K(fixeddof,fixeddof);
Kff=K(freedof,freedof);

Kpf=K(fixeddof,freedof);
Kfp=K(freedof,fixeddof);

K=[Kpp Kpf;
    Kfp Kff];
end

