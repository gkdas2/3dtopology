function [C,Ceq,DC,DCeq]=nonlcon(rho,Wbar)
%Constraints

rhophys=Wbar*rho'; %Filter density
vole=12/120*2/20*0.1*ones(1,2400);  %Volume of each element
Volfrac=0.4;

C=sum(rhophys'.*vole)-Volfrac*sum(vole); %Inequality constraint on volume
% DC=Wbar*vole';      %Convert filtered sensitivity back to actual
DC=Wbar*vole';
Ceq=[];
DCeq=[];

end
