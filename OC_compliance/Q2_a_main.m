clc
clear ;close all;
format short
%----------------Evaluate Optimal Area-------------------%
rho=0.1*ones(1,2400);     %design variables: rho 
lb=1e-4*ones(1,2400);     % lower bound 
ub=1*ones(1,2400);              %Upper limit
A=[];
b=[];
Aeq=[];
beq=[];

disp('AE498:SDO, HW4, Q2(a)...GHANENDRA KUMAR DAS...')

%------------Prepare Domain---------%
r=0.15;     %Filter radius
disp('filter radius=')
disp(r)

%Total Length in X direction,m
totalLengthx=12 ;

%Tolotal Length in Y direction,m
totalHeight=2 ;     
     
%Young's Modulus, KN/m2
E0=75e3 ;  

%Poisson ratio
mu=0.3  ;    

%---------------------------------120x20 mesh-----------------------%
numElementsX=120;    %Number of Elements in X direction
numElementsY=20;     %Number of Elements in Y direction
t=0.1;

numElements=numElementsX*numElementsY;      %Total number of Elements
numNodes=(numElementsX+1)*(numElementsY+1); %Total number of Elements
nodeIndex=1:numNodes;                       %Index of all nodes

dofIndex=1:2*numNodes;                      %Index of all dofs


eleLength=totalLengthx/numElementsX;        %Individual Element length in X direction 
eleWidth=totalHeight/numElementsY;          %Individual Element lenght in Y direction
                                  
disp('Generating node coordinates...')
x=0:eleLength:totalLengthx;
y=0:eleWidth:totalHeight;


[y,x]=meshgrid(y,x);
 
x=x(:);
y=y(:);
 
COORD=[x,y];                               %Coordinates of nodes bottom left to top right



%-----------Generate Element connectivity matrix map---------
disp('Generating 2D node arrangements...')
[mat,map]=Connectivity2D(nodeIndex,numElementsX,numElementsY);  % mat=2D node arrangement, map=2D conectivity matrix


disp('Calculating centriod of each element...') 
centroid=zeros(numElements,2);

for i=1:numElements
    nodes=map(i,:);   %extract nodes for each element from connectivity matrix
    gcoord=COORD(nodes,:);
    centroid(i,:)=mean(gcoord);
end
  
 disp('Generating density filter multiplier matrix...')
% W=zeros(numElements);
Wbar=zeros(numElements);
% for i=1:numElements
%     for j=1:numElements
%         distance= sqrt(sum((centroid(i,:)-centroid(j,:)).^2));
%         W(i,j)=max(0,r-distance);
%     end
%     Wbar(i,:)=W(i,:)/sum(W(i,:));    
%     
% end

D=pdist(centroid);
rdist=squareform(D);
WW=max(0,r-rdist);
row_sums=sum(WW,2);

for i=1:size(WW,2)
Wbar(i,:)=WW(i,:)/row_sums(i);
end
Wbar=(Wbar);

disp('Being optimization...')

% Define optimization options
options = optimoptions(@fmincon,'Algorithm','interior-point','MaxIter',5000,'MaxFunEvals',5000,'TolFun',1e-5,'TolX',1e-7,'GradObj','on','GradConstr','on','Display','iter','PlotFcn','optimplotfval'); %'PlotFcn','optimplotx' for design variables

f1=@(rho) fun(rho,Wbar,totalLengthx,numElementsX,totalHeight,numElementsY,t,E0,mu,COORD,mat,map); %Objective function
f2=@(rho)nonlcon(rho,Wbar); %Gradient function

%Evaluate optimal value and optimal function
[rhoopt,fopt]=fmincon(f1,rho,A,b,Aeq,beq,lb,ub,f2,options);

%-Display geometry--------------%
rhomatrix=reshape(rhoopt,120,20);
rhomatrix=rhomatrix';
 rhomatrix=flip(rhomatrix);
figure
colormap(gray)
imagesc(-rhomatrix)
colorbar;
title('Optimized density')

rhoopt_Phys=Wbar*rhoopt';
rhomatrix=reshape(rhoopt_Phys,120,20);
rhomatrix=rhomatrix';
 rhomatrix=flip(rhomatrix);
figure
colormap(gray)
imagesc(-rhomatrix)
colorbar;
title('Optimized Physical density(filtered)')