function [uobj,grad]=PlaneStress_adjoint(rho,~,numElementsX,~,numElementsY,t,E0,mu,COORD,mat,map)
% %------------------------------------------------------------------------%
%AE 498: SDO,HW 4, Spring 2020

%Ghanendra Kumar Das,AE, UIUC

%------------------------------------------------------------------------%


format long
p=3;  %Penalty
numElements=numElementsX*numElementsY;      %Total number of Elements
numNodes=(numElementsX+1)*(numElementsY+1); %Total number of Elements
                 

dofIndex=1:2*numNodes;                      %Index of all dofs

Emin=1e-9;
 

% -------Material Properties---------
%  E0=150e9;  % Young's Modulus, N/m2
%  mu=0.3;    %Poisson ratio
E=E0/(1-mu^2)*[1 mu 0;                      %Plane stress stiffness matrix
              mu 1 0;
             0 0 (1-mu)/2];


%%

[K,U,F]=InitializeFE(numNodes,2);           % 2D Initialization

ke0(:,:,numElements)=zeros(8);
ke(:,:,numElements)=zeros(8);
%----------Begin calculating K matrix for FE Analysis--------------%
for i=1:numElements
   nodes=map(i,:);   %extract nodes for each element from connectivity matrix
   gcoord=COORD(nodes,:);    %extract corresponding (x,y) coordinates for the nodes
 thickness=t;
  rhoi=rho(i);
[~,B1,~,det_J1]=shapefunction2D(-sqrt(1/3),-sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [~,B2,~,det_J2]=shapefunction2D(sqrt(1/3),-sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [~,B3,~,det_J3]=shapefunction2D(sqrt(1/3),sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [~,B4,~,det_J4]=shapefunction2D(-sqrt(1/3),sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
  ke01=thickness*(B1'*Emin*B1*det_J1+B2'*Emin*B2*det_J2+B3'*Emin*B3*det_J3+B4'*Emin*B4*det_J4); %Evaluate total integral via Gauss rule, weights w1=w2=1       %partial diff ke wrt ti
ke0(:,:,i)=thickness*(B1'*(E-Emin)*B1*det_J1+B2'*(E-Emin)*B2*det_J2+B3'*(E-Emin)*B3*det_J3+B4'*(E-Emin)*B4*det_J4); %Evaluate total integral via Gauss rule, weights w1=w2=1       %partial diff ke wrt ti
 
%   ke0(:,:,i)=thickness*(B1'*(Emin+rhoi*(E-Emin)*B1*det_J1+B2'*(E-Emin)*B2*det_J2+B3'*(E-Emin)*B3*det_J3+B4'*(E-Emin)*B4*det_J4); %Evaluate total integral via Gauss rule, weights w1=w2=1       %partial diff ke wrt ti
% ke=ke0(:,:,i);
  ke(:,:,i)=ke01+rhoi^p*ke0(:,:,i);
  index=[2*nodes-1;           %corresponding u,v dofs for the current nodes
        2*nodes];
    index=index(:);

K(index,index)=K(index,index)+ke(:,:,i);     %Insert local ke in Global K matrix at correct position
end
 Py=-100; %KN/m

 UDL=[2338 0 Py 0 1 3;
     2339 0 Py -1 1 3;
    2340 0 Py -1 1 3;
    2341 0 Py -1 1 3;
     2342 0 Py -1 1 3;
    2343 0 Py -1 0 3]; %[Element number, Px, Py,staring point, End point, Face]
% 



fixednodes=[mat(:,1);mat(:,end)];     %Fixednodes: Leftmost nodes in 2D arrangements             
          
fixednodes=fixednodes(:)';
fixeddof=[2*fixednodes-1,2*fixednodes];
U(fixeddof,1)=0;                %Apply fixed value to fixed dofs
 
freedof=setdiff(dofIndex,fixeddof);

for i=1:size(UDL)
    elnum=UDL(i,1);     %Element number on which UDL acts
    nodes=map(elnum,:);   %extract nodes for the element element from connectivity matrix
   gcoord=COORD(nodes,:);    %extract corresponding (x,y) coordinates for the nodes
L=t*UDL2D(UDL(i,2:3),UDL(i,4),UDL(i,5),UDL(i,6),gcoord); %Load vector multiplied by thickness

index=[2*nodes-1;           %corresponding u,v dofs for the current nodes
        2*nodes];
index=index(:); 
F(index)=F(index)+L;
end
K=(K+K')/2;
%... Partition K matrix into blocks........%
[Kpp,Kpf,Kfp,Kff]= Partition2D(K,fixeddof,freedof);

Up=U(fixeddof);
Ff=F(freedof);

%.....Calculate state variable values using FE.............%
Uf=Kff\(Ff-Kfp*Up);
Fp=Kpf*Uf+Kpp*Up;
U(freedof)=Uf;
F(fixeddof)=Fp;

%......Objective is deflection at bottom right corner..............%
% objnode=2481;
% uobj=abs(U(objnode*2,1));

%.......Calculate partial diff of objective function wrt state
%variables...%
% L=zeros(2*numNodes,1);
% L(2*objnode)=abs(U(2*objnode))/U(2*objnode);



% Caclulate adjoint vector once for each f function

% psi(fixeddof,1)=0;
% psi(freedof,1)=-u(freedof,1);

%...Calculate gradient for ith design variable associated with ith element
grad=zeros(numElements,1);
uobj=0;
for i=1:numElements
    nodes=map(i,:);   %extract nodes for each element from connectivity matrix
   index=[2*nodes-1;           %corresponding u,v dofs for the current nodes
        2*nodes];
    index=index(:);
    
    rhoi=rho(i);
    keoi=ke0(:,:,i);
    kei=ke(:,:,i);
    ui=U(index,1);
    uobj=uobj+ui'*kei*ui;
    grad(i)=-p*rhoi^(p-1)*ui'*keoi*ui;
end

end


