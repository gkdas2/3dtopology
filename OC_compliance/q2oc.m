%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2D Topology Optimization
% Optimality Condition method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%..........GHANENDRA KUMAR DAS, AE ,UIUC......
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

clc
clear ;close all;
format short
%----------------Evaluate Optimal Area-------------------%
rho=0.4*ones(2400,1);     %design variables: rho 

disp('AE498:SDO, HW4, Q2(a)...GHANENDRA KUMAR DAS...')

%------------Prepare Domain---------%
r=0.2;     %Filter radius
disp('filter radius=')
disp(r)

%Total Length in X direction,m
totalLengthx=12 ;

%Tolotal Length in Y direction,m
totalHeight=2 ;     
     
%Young's Modulus, KN/m2
E0=75e3 ;  

%Poisson ratio
mu=0.3  ;    

%---------------------------------120x20 mesh-----------------------%
numElementsX=120;    %Number of Elements in X direction
numElementsY=20;     %Number of Elements in Y direction
t=0.1;  %thickness

numElements=numElementsX*numElementsY;      %Total number of Elements
numNodes=(numElementsX+1)*(numElementsY+1); %Total number of Elements
nodeIndex=1:numNodes;                       %Index of all nodes

dofIndex=1:2*numNodes;                      %Index of all dofs


eleLength=totalLengthx/numElementsX;        %Individual Element length in X direction 
eleWidth=totalHeight/numElementsY;          %Individual Element lenght in Y direction
                                  
disp('Generating node coordinates...')
x=0:eleLength:totalLengthx;
y=0:eleWidth:totalHeight;


[y,x]=meshgrid(y,x);
 
x=x(:);
y=y(:);
 
COORD=[x,y];                               %Coordinates of nodes bottom left to top right



%-----------Generate Element connectivity matrix map---------
disp('Generating 2D node arrangements...')
[mat,map]=Connectivity2D(nodeIndex,numElementsX,numElementsY);  % mat=2D node arrangement, map=2D conectivity matrix


disp('Calculating centriod of each element...') 
centroid=zeros(numElements,2);

for i=1:numElements
    nodes=map(i,:);   %extract nodes for each element from connectivity matrix
    gcoord=COORD(nodes,:);
    centroid(i,:)=mean(gcoord);
end
  
 disp('Generating density filter multiplier matrix...')
% W=zeros(numElements);


D=pdist(centroid);
rdist=squareform(D);
WW=max(0,r-rdist);
row_sums=sum(WW,2);

Wbar=zeros(numElements); %Denstity filter matrix
for i=1:size(WW,2)
Wbar(i,:)=WW(i,:)/row_sums(i);
end


vole=12/120*2/20*0.1*ones(1,2400);  %Volume of each element
Volfrac=0.4;
Fullvol=Volfrac*sum(vole);      %Domain volume
disp('Begin Optimization...')

loop = 0; 
change = 1.;
% START ITERATION
while change > 0.01  
  loop = loop + 1;
  rhoPhys = rho;
  [Obj,gradphys]=PlaneStress_adjoint(rhoPhys,totalLengthx,numElementsX,totalHeight,numElementsY,t,E0,mu,COORD,mat,map); %FE Analysis
   gradd=Wbar*gradphys;  %Convert filtered sensitivity back to actual
    
   DC=Wbar*vole'; %Convert filtered sensitivity back to actual
    
   % Density Update by Optimality Critera method
   l1 = 0; l2 = 1e9; move = 0.2;
   while (l2-l1)/(l1+l2) > 1e-3
    lmid = 0.5*(l2+l1);
    rhonew= max(0,max(rho-move,min(1,min(rho+move,rho.*sqrt(-gradd./DC/lmid)))));

    rhoPhys= Wbar*rhonew; %Filter density

    if sum(rhoPhys.*vole')-Fullvol>0 
    l1 = lmid; 
    else
    l2 = lmid;
    end
   end
   
    change = max(abs(rhonew(:)-rho(:))); %change in density parameter
    rho = rhonew; %Update rho for next iteration

    % Plot density 
    myfig=figure(1);
    rhomatrix=reshape(rhoPhys,120,20);
    rhomatrix=rhomatrix';
    rhomatrix=flip(rhomatrix);
    colormap(gray)
    imagesc(-rhomatrix)
    colorbar;
    drawnow
    hold on
    axis equal

    %Display progress
    fprintf(' It.:%5i  Obj.:%11.4f    Vol.:%7.3f    ch.:%7.3f\n',loop,Obj, ...
    mean(rhoPhys(:)),change);
end

disp('Optimization converged')

title('Optimized design')