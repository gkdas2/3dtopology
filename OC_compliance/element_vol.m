clc;

map=[1 2 3 4];
COORD=[ 2 0;
        4 0;
        5 5;
        3 3];
  for i=1:1
   nodes=map(i,:);   %extract nodes for each element from connectivity matrix
   gcoord=COORD(nodes,:);    %extract corresponding (x,y) coordinates for the nodes
 thickness=t;
  rhoi=rho(i);
[N1,B1,J1,det_J1]=shapefunction2D(-sqrt(1/3),-sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [N2,B2,J2,det_J2]=shapefunction2D(sqrt(1/3),-sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [N3,B3,J3,det_J3]=shapefunction2D(sqrt(1/3),sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [N3,B4,J4,det_J4]=shapefunction2D(-sqrt(1/3),sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
%   ke01=thickness*(B1'*Emin*B1*det_J1+B2'*Emin*B2*det_J2+B3'*Emin*B3*det_J3+B4'*Emin*B4*det_J4); %Evaluate total integral via Gauss rule, weights w1=w2=1       %partial diff ke wrt ti
% ke0(:,:,i)=thickness*(B1'*(E-Emin)*B1*det_J1+B2'*(E-Emin)*B2*det_J2+B3'*(E-Emin)*B3*det_J3+B4'*(E-Emin)*B4*det_J4); %Evaluate total integral via Gauss rule, weights w1=w2=1       %partial diff ke wrt ti
%  
% %   ke0(:,:,i)=thickness*(B1'*(Emin+rhoi*(E-Emin)*B1*det_J1+B2'*(E-Emin)*B2*det_J2+B3'*(E-Emin)*B3*det_J3+B4'*(E-Emin)*B4*det_J4); %Evaluate total integral via Gauss rule, weights w1=w2=1       %partial diff ke wrt ti
% % ke=ke0(:,:,i);
%   ke(:,:,i)=ke01+rhoi^p*ke0(:,:,i);
  
end  
det_J1+det_J2+det_J3+det_J4
 

a=sqrt(10);
b=sqrt(26);
c=sqrt(8);
s=(a+b+c)/2;
A=sqrt(s*(s-a)*(s-b)*(s-c))