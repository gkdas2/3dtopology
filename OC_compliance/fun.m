function [Obj,gradd]=fun(rho,Wbar,totalLengthx,numElementsX,totalHeight,numElementsY,t,E0,mu,COORD,mat,map);


rhophys=Wbar*rho'; %Filter density
[Obj,gradphys]=PlaneStress_adjoint(rhophys,totalLengthx,numElementsX,totalHeight,numElementsY,t,E0,mu,COORD,mat,map);
gradd=Wbar*gradphys;  %Convert filtered sensitivity back to actual


%----For plotting each iteration-----% 
myfig=figure(1);
rhomatrix=reshape(rhophys,120,20);
rhomatrix=rhomatrix';
 rhomatrix=flip(rhomatrix);
colormap(gray)
imagesc(-rhomatrix)
colorbar;
drawnow


end


