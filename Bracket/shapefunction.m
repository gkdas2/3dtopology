function [B,det_J,N_expanded]=shapefunction(zeta,eta,xi,gcoord)


N=1/8*[(1-zeta)*(1-eta)*(1-xi), (1+zeta)*(1-eta)*(1-xi),(1+zeta)*(1+eta)*(1-xi),(1-zeta)*(1+eta)*(1-xi),...
    (1-zeta)*(1-eta)*(1+xi), (1+zeta)*(1-eta)*(1+xi), (1+zeta)*(1+eta)*(1+xi),(1-zeta)*(1+eta)*(1+xi)];

DN=1/8*[-1*(1-eta)*(1-xi), (1-eta)*(1-xi),(1+eta)*(1-xi),(1+eta)*(1-xi)*-1,...
    -1*(1-eta)*(1+xi), (1-eta)*(1+xi),(1+eta)*(1+xi),(1+eta)*(1+xi)*-1;
    -1*(1-zeta)*(1-xi), (1+zeta)*(1-xi)*-1,(1+zeta)*(1-xi),(1-zeta)*(1-xi),...
    -1*(1-zeta)*(1+xi), (1+zeta)*(1+xi)*-1,(1+zeta)*(1+xi),(1-zeta)*(1+xi);
    -1*(1-zeta)*(1-eta), (1+zeta)*(1-eta)*-1,(1+zeta)*(1+eta)*-1,(1-zeta)*(1+eta)*-1,...
    (1-zeta)*(1-eta), (1+zeta)*(1-eta),(1+zeta)*(1+eta),(1-zeta)*(1+eta)];

J=DN*gcoord;    %Jacobian of transformation
det_J=det(J);   %determinant of Jacobian
DG=J\DN;    %Derivative in global coordinates x,y

 B=zeros(6,24);
 
 B(1,1:3:24)=DG(1,:);
 B(2,2:3:24)=DG(2,:);  
 B(3,3:3:24)=DG(3,:);
 
  B(4,2:3:24)=DG(3,:); 
  B(4,3:3:24)=DG(2,:);
  
 B(5,1:3:24)=DG(3,:); 
 B(5,3:3:24)=DG(1,:);
 
 B(6,1:3:24)=DG(2,:);
 B(6,2:3:24)=DG(1,:);

N_expanded=zeros(3,8);
N_expanded(1,1:3:24)=N;
N_expanded(2,2:3:24)=N;
N_expanded(3,3:3:24)=N;
 
 
 
 

