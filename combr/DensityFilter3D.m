function Wbars=DensityFilter3D(map,COORD,r,q)
%----Calculates centroid and density filter matrix for 3D hex elements--%
%Input...(connectivity matrix,coordinates)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...GHANENDRA KUMAR DAS, AE, UIUC...%
%.............................................................%

nElements=size(map,1);


centroid=zeros(nElements,3);

for i=1:nElements
    nodes=map(i,:);   %extract nodes for each element from connectivity matrix
    gcoord=COORD(nodes,:);
    centroid(i,:)=mean(gcoord);
end
  
D=pdist(centroid);
rdist=squareform(D);
WW=(max(0,r-rdist))^q;
row_sums=sum(WW,2);

Wbar=zeros(nElements); %Denstity filter matrix
for i=1:size(WW,2)
Wbar(i,:)=WW(i,:)/row_sums(i);
end

Wbars=sparse(Wbar);
end
