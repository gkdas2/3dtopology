function [uobj,grad,vole]=FEA3D(map,COORD,fixednodes,F,nElements,nNodes, Emin,E0,mu,rho,p) 
%...Performs 3D Linear FEA using unstructured 8 node  hex elements (C3D8 in Abaqus)...%
%...Also Calculates compliance, sensitivity and element volume.....%
%..............GHANENDRA KUMAR DAS, AE, UIUC..............................%


DM=1/((1+mu)*(1-2*mu))*[1-mu mu mu 0 0 0;
                         mu 1-mu mu 0 0 0;
                         mu mu 1-mu 0 0 0;
                         0 0 0 (1-2*mu)/2 0 0;
                         0 0 0 0 (1-2*mu)/2 0;
                         0 0 0 0 0 (1-2*mu)/2];

dof=3;
ndof=1:dof*nNodes;

%------Initialize-----------

K=sparse(nNodes*dof,nNodes*dof);
U=sparse(nNodes*dof,1);
vole=zeros(nElements,1);

%-------free and fixed dofs-------
fixeddof=[3*fixednodes;3*fixednodes-1;3*fixednodes-2];
fixeddof=fixeddof(:);
freedof=setdiff(ndof,fixeddof);
U(fixeddof,1)=0;

%----------------------
ke0(:,:,nElements)=zeros(24);
ke(:,:,nElements)=zeros(24);


%----------Begin calculating K matrix for FE Analysis--------------%
% tic
for i=1:nElements
  nodes=map(i,:);  %extract nodes for each element from connectivity matrix
  gcoord=COORD(nodes,:);    %extract corresponding (x,y) coordinates for the nodes
   rhoi=rho(i);
  [B1,det_J1,~]=shapefunction(-sqrt(1/3),-sqrt(1/3),-sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [B2,det_J2,~]=shapefunction(sqrt(1/3),-sqrt(1/3),-sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [B3,det_J3,~]=shapefunction(sqrt(1/3),sqrt(1/3),-sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [B4,det_J4,~]=shapefunction(-sqrt(1/3),sqrt(1/3),-sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
 [B5,det_J5,~]=shapefunction(-sqrt(1/3),-sqrt(1/3),sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [B6,det_J6,~]=shapefunction(sqrt(1/3),-sqrt(1/3),sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [B7,det_J7,~]=shapefunction(sqrt(1/3),sqrt(1/3),sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [B8,det_J8,~]=shapefunction(-sqrt(1/3),sqrt(1/3),sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
w=1; %weight
% ke01=w*B1'*Emin*B1*det_J1+w*B2'*Emin*B2*det_J2+w*B3'*Emin*B3*det_J3+w*B4'*Emin*B4*det_J4+...
%      w*B5'*Emin*B5*det_J5+w*B6'*Emin*B6*det_J6+w*B7'*Emin*B7*det_J7+w*B8'*Emin*B8*det_J8;
 ke0(:,:,i)=w*B1'*(DM)*B1*det_J1+w*B2'*(DM)*B2*det_J2+w*B3'*(DM)*B3*det_J3+w*B4'*(DM)*B4*det_J4+...
     w*B5'*(DM)*B5*det_J5+w*B6'*(DM)*B6*det_J6+w*B7'*(DM)*B7*det_J7+w*B8'*(DM)*B8*det_J8;

 ke(:,:,i)=(Emin+rhoi^p*(E0-Emin))*ke0(:,:,i);
index=[3*nodes-2;           %corresponding u,v dofs for the current nodes
        3*nodes-1;
        3*nodes];
    index=index(:);
    K(index,index)=K(index,index)+ke(:,:,i);     %Insert local ke in Global K matrix at correct position
vole(i)=abs(det_J1+det_J2+det_J3+det_J4+det_J5+det_J6+det_J7+det_J8);    
end
% t1=toc;

%----------FE Analysis------------------

Kff=sparse(K(freedof,freedof));
U(freedof,1)=Kff\F(freedof);

grad=zeros(nElements,1);
uobj=0;
% tic
for i=1:nElements
    nodes=map(i,:);  %extract nodes for each element from connectivity matrix
   index=[3*nodes-2;           %corresponding u,v dofs for the current nodes
        3*nodes-1;
        3*nodes];
    index=index(:);
    
    rhoi=rho(i);
    keoi=ke0(:,:,i);
    kei=ke(:,:,i);
    ui=U(index,1);
    uobj=uobj+ui'*kei*ui;
    grad(i)=-p*rhoi^(p-1)*ui'*keoi*ui;
end
% t1=toc;

% for ii=901:1012
%     nodess=map(i,:);
%     t=1;
%     f=t*N5(:,:,i)'*P+t*N6(:,:,i)'*P+t*N7(:,:,i)'*P+t*N8(:,:,i)'*P;
% indexx=[];
%   for jj=1:8
%       indexx=[indexx,3*nodess(jj)-2,3*nodess(jj)-1,3*nodess(jj)];  %Determine correct positiom for the ith element's local ke in Global K matrix
%   end
   
%    F(indexx,1)=F(indexx,1)+f;
% end
    
    
