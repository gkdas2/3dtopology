clc
clear


format long
load coord.m
load connect.m
load fixednodes.m
load cload.m

E0=100;
mu=0.3;

E=E0/((1+mu)*(1-2*mu))*[1-mu mu mu 0 0 0;
                         mu 1-mu mu 0 0 0;
                         mu mu 1-mu 0 0 0;
                         0 0 0 (1-2*mu)/2 0 0;
                         0 0 0 0 (1-2*mu)/2 0;
                         0 0 0 0 0 (1-2*mu)/2];
%----------arrange data------------

elIndex=connect(:,1)';
nElements=size(elIndex',1);

nodeIndex=coord(:,1)';
nNodes=size(nodeIndex',1);

dof=3;%per node
ndof=1:dof*nNodes;

COORD= coord(:,[2 3 4]);

map=connect(:,2:9);
cloadnode=cload(:,1);
cloaddof=cload(:,2);
cloadmag=cload(:,3);
%Visualization
% [t,tnorm]=MyRobustCrust(COORD);
% hold on; 
% trisurf(t,COORD(:,1),COORD(:,3),COORD(:,2),'facecolor','k','edgecolor','c'),view(-7,3);
% axis tight
% grid on
% xlabel('x');
% ylabel('z');
% zlabel('y');
% title('Original shape');
% hold off
% figure
% title('Geometry- not representative of actual hex mesh','fontsize',14);
% trisurf(t,COORD(:,1),COORD(:,2),COORD(:,3),'facecolor','k','edgecolor','c'),view(0,-100);
% grid on
% figure
% nodes=map(:,:);
% gcoord=COORD(nodes,:);
% plot3(gcoord(:,1),gcoord(:,2),gcoord(:,3),'o','color','k'),view (120,120);
% grid on
% axis 

%------Initialize-----------

K=zeros(nNodes*dof,nNodes*dof);
U=sparse(nNodes*dof,1);
F=sparse(nNodes*dof,1);

%-------free and fixed dofs-------
fixeddof=[3*fixednodes;3*fixednodes-1;3*fixednodes-2];
fixeddof=sort(fixeddof)';
freedof=setdiff(ndof,fixeddof);
U(fixeddof,1)=0;

%----------------------


disp('Begin stiffness calculation')
%----------Begin calculating K matrix for FE Analysis--------------%
tic
for i=1:nElements
  nodes=map(i,:);  %extract nodes for each element from connectivity matrix
  gcoord=COORD(nodes,:);    %extract corresponding (x,y) coordinates for the nodes
  [B1,det_J1,N1]=shapefunction(-sqrt(1/3),-sqrt(1/3),-sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [B2,det_J2,N2]=shapefunction(sqrt(1/3),-sqrt(1/3),-sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [B3,det_J3,N3]=shapefunction(sqrt(1/3),-sqrt(1/3),sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [B4,det_J4,N4]=shapefunction(sqrt(1/3),sqrt(1/3),-sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
 [B5,det_J5,N5]=shapefunction(-sqrt(1/3),sqrt(1/3),-sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
  [B6,det_J6,N6]=shapefunction(sqrt(1/3),sqrt(1/3),-sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
  [B7,det_J7,N7]=shapefunction(sqrt(1/3),sqrt(1/3),sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
  [B8,det_J8,N8]=shapefunction(-sqrt(1/3),sqrt(1/3),sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
t=1; %weight
 ke=t*B1'*E*B1*det_J1+t*B2'*E*B2*det_J2+t*B3'*E*B3*det_J3+t*B4'*E*B4*det_J4+...
     t*B5'*E*B5*det_J5+t*B6'*E*B6*det_J6+t*B7'*E*B7*det_J7+t*B8'*E*B8*det_J8;
 
index=[3*nodes-2;           %corresponding u,v dofs for the current nodes
        3*nodes-1;
        3*nodes];
    index=index(:);
    K(index,index)=K(index,index)+ke;     %Insert local ke in Global K matrix at correct position
    
end
t1=toc
disp('stiffness matrix assembled')

F([2 17 56 62],1)=10;

%----------FE Analysis------------------
tic;
Kff=sparse(K(freedof,freedof));
U(freedof,1)=Kff\F(freedof);
% 
% 
% 
% 
t2=toc


[m,i]=max(abs(U))








% for ii=901:1012
%     nodess=map(i,:);
%     t=1;
%     f=t*N5(:,:,i)'*P+t*N6(:,:,i)'*P+t*N7(:,:,i)'*P+t*N8(:,:,i)'*P;
% indexx=[];
%   for jj=1:8
%       indexx=[indexx,3*nodess(jj)-2,3*nodess(jj)-1,3*nodess(jj)];  %Determine correct positiom for the ith element's local ke in Global K matrix
%   end
   
%    F(indexx,1)=F(indexx,1)+f;
% end
    
    
   
% hold on
%     
% %----------FE Analysis------------------
% U(freedof,1)=K(freedof,freedof)\F(freedof);
% %-----------
% xnew=COORD(:,1)+U(1:3:max(size(U)));      %determine new x coordinates of the nodes
% ynew=COORD(:,2)+U(2:3:max(size(U)));      %determine new y coordinates of the nodes
% znew=COORD(:,3)+U(3:3:max(size(U))); 
% 
% COORD_NEW=[xnew, ynew, znew];
% %Visualisation
% 
% [t,tnorm]=MyRobustCrust(COORD_NEW);
% hold on; 
% 
% figure
% trisurf(t,COORD_NEW(:,1),COORD_NEW(:,3),COORD_NEW(:,2),'facecolor','k','edgecolor','r'),view(-7,3);
% grid on
% xlabel('x');
% ylabel('z');
% zlabel('y');
% title('Deformed shape');
% axis tight