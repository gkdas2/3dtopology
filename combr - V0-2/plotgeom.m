function plotgeom(map,COORD,nElements,rho)
 a=COORD(:,2);
 COORD(:,2)=-COORD(:,3);
 COORD(:,3)=a;
 f=[1 2 3 4;
    2 6 7 3;
    4 3 7 8;
    1 5 8 4;
    1 2 6 5;
    5 6 7 8];
figure()
for nn=1:nElements 
    connect=map(nn,:);
    c=rho(nn);
    
    for i=1:6
      if c>1
         c=0.9999999;
      elseif c<0.5
          c=0;
         
     end
    index=f(i,:);
    Faces=connect(index);
    patch('Faces',Faces,'Vertices',COORD,'EdgeAlpha',c,'facealpha',c,'facecolor',0.2*[c c c])
   
   hold on
    end
    hold on
    
    
end
axis equal; axis tight; axis on; box on; view([130,48]);
   xlabel('x')
ylabel('z')
zlabel('y')


end





