clc;
clear

clc
clear ;close all;
format short
%----------------Load Mesh Data-------------------%
disp('Loading mesh...');
load coord.m
load connect.m
load fixednodes.m
load cload.m


%------------Prepare Domain---------%
r=0.2;     %Filter radius
disp('filter radius=')
disp(r)



E0=70e9;
mu=0.3;
Emin=1e-9;

                    





COORD= coord(:,[2 3 4]);
nNodes=size(COORD,1);
dof=3;%per node
ndof=1:dof*nNodes;
map=connect(:,2:9);
nElements=size(map,1); 
rho=rand(nElements,1);
rho=rho/max(rho);

 f=[1 2 3 4;
    5 6 7 8;
    2 3 7 6;
    1 4 8 5;
    3 4 8 7;
    1 2 6 5];
for nn=1:nElements 
    connect=map(nn,:);
    c=rho(nn);
    for i=1:6
     
    index=f(i,:);
    Faces=connect(index);
    patch('Faces',Faces,'Vertices',COORD,'Facecolor',(1-c)*[1 1 1])
   
   hold on
    end
    hold on
    
    
    end




