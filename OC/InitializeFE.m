%Initialize K,U,F matrix for given dof
%Ghanendra Kumar Das, AE, UIUC

function [K,U,F]=InitializeFE(numNodes,dof)
K=sparse(numNodes*dof,numNodes*dof); %Stiffness matrix
    U=zeros(numNodes*dof,1);         %Displacement vector
    F=zeros(numNodes*dof,1);        %Force vector
end